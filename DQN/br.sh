#!/usr/bin/fish

./build.sh
if [ $status -eq 0 ]
	./run.sh
	exit 0
else
	printf "build failed. exiting..\n"
	exit 1
end
