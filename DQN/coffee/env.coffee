DQNAgent = require './DQNAgent'

ask = require("readline-sync").question

deepqlearn = require './deepqlearn'

fs = require 'fs'

random_int = (max,min=0) ->
    return Math.floor(Math.random() * Math.floor(max-min)) + min

log = (data,noSlashN=undefined) ->
    d = ""
    if (typeof(data) == typeof(""))
        d=data
    else
        d=JSON.stringify(data)
    if (noSlashN == undefined and d[d.length-1] != "\n")
        d += "\n"
    process.stdout.write(d)

class V2
    constructor: (x=0,y=0) ->
        @x = x
        @y = y
        
    eq: (v_other) ->
        return v_other? and v_other.x == @x and v_other.y == @y # if exists and values equal
    
    addv: (v_other) ->
        @x += v_other.x
        @y += v_other.y
        return @clone()
    
    addn: (x,y) ->
        @x += x
        @y += y
        return @clone()
    
    clone: () ->
        return new V2(@x,@y)
    
    cap: (max_x,min_x,max_y,min_y) ->
        @x = Math.min(Math.max(min_x, @x), max_x) # clamp x value
        if max_y != "undefined" and min_y != "undefined" # if no y value
            @y = Math.min(Math.max(min_y, @y), max_y) # clamp y value using x clamp
        else # has y values
            @y = Math.min(Math.max(min_x, @y), max_x) # clamp y value
            
    dist: (v_other) ->
        return Math.sqrt( ( @x - v_other.x )**2 + ( @y - v_other.y )**2 ) # pythagoras!
            
    @rand: (max_x,min_x,max_y,min_y) ->
        x = random_int(max_x,min_x) # rand x
        y = random_int(max_y,min_y) # rand y
        return new V2(x,y) # ret new vec
    
    @rand_diff: (max_x,min_x,max_y,min_y,vec) ->
        v = V2.rand(max_x,min_x,max_y,min_y) # new vec
        while (v.eq(vec)) # if same
            v = V2.rand(max_x,min_x,max_y,min_y) # new vec
        return v # ret
    
    @rand_diff_list: (max_x,min_x,max_y,min_y,l) ->
        check = (v,list) ->
            for vec in list
                if vec.eq(v)
                    return true
            return false
        
        v = V2.rand(max_x,min_x,max_y,min_y)
        while (check(v,l))
            v = V2.rand(max_x,min_x,max_y,min_y)
        return v
    
    str: ->
        return "(#{@x},#{@y})"
        

class blob
    constructor: (pos,area) ->
        @pos = pos
        @max_width=area.width
        @max_height=area.height
        
    move: (x_v,y) ->
        if typeof(x_v) == typeof(@pos)           
            @pos.addv(x_v)
        else
            @pos.addn(x_v,y)
            
        @pos.cap(@max_width,0,@max_height,0)
            
    act: (action) ->
        switch action
            when 0 then @move(0,1) # ^
            when 1 then @move(0,-1) # \/
            when 2 then @move(1,0) # >
            when 3 then @move(-1,0) # <
            when 4 then @move(1,1) # />
            when 5 then @move(1,-1) # \>
            when 6 then @move(-1,1) # \<
            when 7 then @move(-1,-1) # /<
            else log("invalid:#{action}")
            
    overlap: (blob_o) ->
        return blob_o.pos.eq(@pos)

class ENV

    constructor: (width,height) ->
        @width=width
        @height=height
        
        @area = {width:width-1,height:height-1}
        
        @max_moves = 200
        
        @food_r = 25
        @move_r = -1
        @death_r = -100
        @reset()
        
    reset: ->
        @food = new blob(V2.rand(width-1,0,height-1,0),@area)
        @enemy = new blob(V2.rand_diff(width-1,0,height-1,0,@food.pos),@area)
        @player = new blob(V2.rand_diff_list(width-1,0,height-1,0,[@food.pos,@enemy.pos]),@area)
        
        @move_count = 0
        

    tick: (action) ->
        @player.act(action)
        done = false
        r = 0
        if @player.overlap(@food)
            r = @food_r
            done = true
        else if @player.overlap(@enemy) or @move_count + 1 >= @max_moves
            r = @death_r
            done = true
        else
            r = @move_r
            
        return
            r:r,
            done:done
    get_state: (distance=true) ->
        ret = null
        if distance
            ret = [@player.pos.dist(@food.pos),@player.pos.dist(@enemy.pos)]
        else
            ret = @get_screen()
        ret = [@player.pos.x,@player.pos.y,@food.pos.x,@food.pos.y,@enemy.pos.x,@enemy.pos.y]
        return ret
                
    get_screen: () ->
        s = []
        for y in [0...@height]
            s.push([])
            for x in [0...@width]
                s[y].push(0)
        #log("player:#{@player.pos.str()},food:#{@food.pos.str()},enemy:#{@enemy.pos.str()},s:#{s}")
        s[@player.pos.y][@player.pos.x] = 1
        s[@food.pos.y][@food.pos.x] = 2
        s[@enemy.pos.y][@enemy.pos.x] = 3        
        return s
    
    get_screen_nice: ->
        s = []
        for y in [0...@height]
            s.push([])
            for x in [0...@width]
                s[y].push("||")
        #log("player:#{@player.pos.str()},food:#{@food.pos.str()},enemy:#{@enemy.pos.str()},s:#{s}")
        s[@player.pos.y][@player.pos.x] = "P "
        s[@food.pos.y][@food.pos.x] = "F "
        s[@enemy.pos.y][@enemy.pos.x] = "E "
        
        for i in [0...s.length]
            s[i]=s[i].join("")
        s=s.join("\n")
        return s

width=20
height=20
use_distance=false

train = (episodes = 100,use_distance=true,width=20,height=20,file_name="net") ->
    
    env = new ENV(width,height)
    
    inp_shape = null
    if use_distance
        inp_shape = [2]
    else
        inp_shape = [width,height]
    
    agent = new DQNAgent(inp_shape,8,3)
    
    for ep in [0...episodes]
        log("ep:#{ep}")
        done = false
        while not done
            state = env.get_state(use_distance)
            action = agent.predict_x(state)
            {r,done} = env.tick(action)
            #log(env.get_state(true))
            agent.backward(r)
        env.reset()
            
    rs = agent.rewards
    acts = agent.actions
    sum_r = 0
    for r in rs
        sum_r += r
        
    sum_r /= rs.length
    log("average_r:#{sum_r}")
    agent.save_net("net")

test_net = (file_name="net") ->
    env = new ENV(width,height)
    
    inp_shape=null
    if use_distance
        inp_shape = [2]
    else
        inp_shape = [width,height]
    
    temp_win = 1;
    num_acts = 8;
    num_inps = 6;
    net_size = temp_win * num_acts + temp_win * num_inps + num_inps;
    opt={
        temporal_window:temp_win,
        experience_size:30000,
        start_learn_threshold: 2000,
        gamma: 0,
        learning_steps_total: 2e9,
        learning_steps_burnin : 1000,
        epsilon_min: 0.01,
        epsilon_test_time:0,
        layer_defs:[
            {type:"input",out_sx:1,out_sy:1,out_depth:net_size},
            {type:"fc",num_neurons:60,activation:"relu"},
            {type:"fc",num_neurons:80,activation:"relu"},
            {type:"regression",num_neurons:8}],
        tdtrainer_options:
            learning_rate:0.001,
            momentum:0.0,
            batch_size:16,
            l2_decay:0.01,
            method:"adam"
    }
    brain = new deepqlearn.Brain(num_inps,num_acts,opt)
    
    t = fs.readFileSync(file_name+".net","utf8")
    brain.value_net.fromJSON(JSON.parse(t))
    
    brain.learning = false
    done = false
    while not done
        log(env.get_screen_nice())
        action = brain.forward(env.get_state())
        log(action+", "+env.get_state())
        {r,done} = env.tick(action)
        brain.backward(r)
        ask "#{r}....\n"
    
    log(env.get_screen_nice())

test_net()
