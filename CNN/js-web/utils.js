
  var argMax, clear_line, log, percentage_bar, random_int, string_times;

  log = function(data, noSlashN = void 0) {
    var d;
    d = "";
    if (typeof data === typeof "") {
      d = data;
    } else {
      d = JSON.stringify(data);
    }
    if (noSlashN === void 0 && d[d.length - 1] !== "\n") {
      d += "\n";
    }
    //process.stdout.write(d)
    return console.log(d);
  };

  string_times = function(chr, count) {
    var i;
    return ((function() {
      var j, ref, results;
      results = [];
      for (i = j = 0, ref = count; (0 <= ref ? j < ref : j > ref); i = 0 <= ref ? ++j : --j) {
        results.push(chr);
      }
      return results;
    })()).join("");
  };

  argMax = function(array) {
    var i, index, j, ref;
    index = 0;
    for (i = j = 0, ref = array.length; (0 <= ref ? j < ref : j > ref); i = 0 <= ref ? ++j : --j) {
      if (array[i] > array[index]) {
        index = i;
      }
    }
    return index;
  };

  clear_line = function() {
    return log("\r\x1b[k");
  };

  percentage_bar = function(total, action) {
    var bar, index, j, perc, perc_width, ref, results, width;
    if (typeof total !== typeof 0 || typeof action !== typeof (function() {})) {
      throw "invalid type";
      return;
    }
    results = [];
    for (index = j = 0, ref = total; (0 <= ref ? j < ref : j > ref); index = 0 <= ref ? ++j : --j) {
      action(index);
      perc = index / total;
      width = (process.stdout.columns || defaultColumns) - 7;
      perc_width = Math.round(perc * width);
      perc = Math.round(perc * 100);
      perc = perc.toString() + "%" + string_times(" ", 3 - perc.toString().length);
      bar = perc + " |" + string_times("#", perc_width) + string_times(" ", width - perc_width) + "|";
      results.push(log("\r" + bar, true));
    }
    return results;
  };

  random_int = function(max, min = 0) {
    return Math.floor(Math.random() * Math.floor(max - min)) + min;
  };
