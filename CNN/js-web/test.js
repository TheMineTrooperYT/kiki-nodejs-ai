
var i, j, layer_defs, net, obj, res, res1, samples, str_res, str_res1, test, trainer;

layer_defs = [];

layer_defs.push({
  type: 'input',
  out_sx: 24,
  out_sy: 24,
  out_depth: 1
});

layer_defs.push({
  type: 'conv',
  sx: 5,
  filters: 8,
  stride: 1,
  pad: 2,
  activation: 'relu'
});

layer_defs.push({
  type: 'pool',
  sx: 2,
  stride: 2
});

layer_defs.push({
  type: 'conv',
  sx: 5,
  filters: 16,
  stride: 1,
  pad: 2,
  activation: 'relu'
});

layer_defs.push({
  type: 'pool',
  sx: 3,
  stride: 3
});

layer_defs.push({
  type: 'softmax',
  num_classes: 10
});

net = new convnetjs.Net();

net.makeLayers(layer_defs);

trainer = new convnetjs.SGDTrainer(net, {
  method: 'adadelta',
  batch_size: 20,
  l2_decay: 0.01
});

test = test_data_js_lim;

samples = 1000;

res = net.getParamsAndGrads();

var RETS = [];
var pred;

for (i = j = 0; j < samples && j < 1000; i = ++j) {
  obj = trainer.train(test[i].data, test[i].class);
  net.forward(test[i].data,true);
  pred = net.getPrediction();
  obj.accur = pred === test[i].class ? 1 : 0;
  RETS.push(obj);
}

res1 = net.getParamsAndGrads();

str_res = JSON.stringify(res);

str_res1 = JSON.stringify(res1);

log(str_res === str_res1);

