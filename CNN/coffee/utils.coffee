log = (data,noSlashN=undefined) ->
    d = ""
    if (typeof(data) == typeof(""))
        d=data
    else
        d=JSON.stringify(data)
    if (noSlashN == undefined and d[d.length-1] != "\n")
        d += "\n"
    process.stdout.write(d)
    #console.log(d)
        
string_times = (chr,count) ->
    return (chr for i in [0...count]).join("")

argMax = (array) ->
    index = 0
    for i in [0...array.length]
        if (array[i] > array[index])
            index = i
    return index
            
clear_line = () ->
    log("\r\x1b[k")
    
percentage_bar = (total,action) ->
    if (typeof(total) != typeof(0) || typeof(action) != typeof((()->)))
        throw "invalid type"
        return
    for index in [0...total]
        action(index)
        perc = index/total
        width = (process.stdout.columns || defaultColumns) - 7
        perc_width = Math.round(perc * width)
        perc = Math.round(perc*100)
        perc = perc.toString() + "%" + string_times(" ",3-perc.toString().length)
        bar = perc+" |" + string_times("#",perc_width) + string_times(" ",width-perc_width) + "|"
        log("\r"+bar,true)
        
random_int = (max,min=0) ->
    return Math.floor(Math.random() * Math.floor(max-min)) + min


module.exports =
    log: log
    string_times: string_times
    argMax: argMax
    clear_line: clear_line
    percentage_bar: percentage_bar
    random_int: random_int