
convnetjs = require './convnet'
fs = require 'fs'
{ argMax,log,string_times,percentage_bar,clear_lines,random_int } = require './utils'

load_train_data = () ->
    dataFileBuffer  = fs.readFileSync '../dataset/train-images-idx3-ubyte'
    labelFileBuffer = fs.readFileSync '../dataset/train-labels-idx1-ubyte'
    pixelValues     = []
    
    for image in [0...60000]
        pixels = []
        for x in [0...28]
            for y in [0...28]
                pixels.push dataFileBuffer[(image * 28 * 28) + (x + (y * 28)) + 15]
            
        pixelValues.push
            data : new convnetjs.Vol(pixels.map(((x) =>
                return x/255
            ))),
            class : labelFileBuffer[image + 8]
            
        #log(pixelValue[image])
    
    return pixelValues

load_test_data = () ->
    dataFileBuffer  = fs.readFileSync '../dataset/t10k-images-idx3-ubyte'
    labelFileBuffer = fs.readFileSync '../dataset/t10k-labels-idx1-ubyte'
    pixelValues     = []
    
    for image in [0...10000]
        pixels = []
        for x in [0...28]
            for y in [0...28]
                pixels.push dataFileBuffer[(image * 28 * 28) + (x + (y * 28)) + 15]
            
        pixelValues.push
            data : new convnetjs.Vol(pixels.map(((x) =>
                return x/255
            ))),
            class : labelFileBuffer[image + 8]
    
    return pixelValues

layer_defs = []
layer_defs.push({type:'input', out_sx:24, out_sy:24, out_depth:1})
layer_defs.push({type:'conv', sx:5, filters:8, stride:1, pad:2, activation:'relu'})
layer_defs.push({type:'pool', sx:2, stride:2})
layer_defs.push({type:'conv', sx:5, filters:16, stride:1, pad:2, activation:'relu'})
layer_defs.push({type:'pool', sx:3, stride:3})
layer_defs.push({type:'softmax', num_classes:10})

net = new convnetjs.Net()
net.makeLayers(layer_defs)

trainer = new convnetjs.SGDTrainer(net, {method:'adadelta', batch_size:20, l2_decay:0.001})


layer_defs = [
    {type:'input', out_sx:28, out_sy:28, out_depth:1},
    {type:'conv', sx:5, filters:16, stride:1, pad:4, activation:"tanh"},
    {type:'pool', sx:2, stride:2},
    {type:'conv', sx:5, filters:16, stride:1, pad:4, activation:"tanh"},
    {type:'pool', sx:2, stride:2},
    {type:'softmax', num_classes:10},
]

net = new convnetjs.Net()

net.makeLayers( layer_defs )

trainer = new convnetjs.Trainer( net, {learning_rate:0.001, method:"adagrad", l1_decay:0.01, batch_size:1} )

train = load_train_data()
test = load_test_data()

###
res = net.getParamsAndGrads()
for i in [0...40]
    obj = trainer.train(train[i]["data"],train[i]["class"])
res1 = net.getParamsAndGrads()
str_res = JSON.stringify(res)
str_res1 = JSON.stringify(res1)
log(str_res == str_res1)
###

xloss_win = []
wloss_win = []
train_acc_win = []

samples = 500

log("Training network.")
losses = []
percentage_bar((if samples <= train.length then samples else train.length),((i)->
    obj = trainer.train(train[i].data,train[i].class)
    losses.push(obj.loss)
    xloss_win.push(obj.cost_loss)
    wloss_win.push(obj.l2_decay_loss)
    train_acc_win.push(net.getPrediction() == train[i].class)
))
log(JSON.stringify(losses))
fs.writeFile("../losses_train.txt",JSON.stringify(losses),((err)->
    if (err)
        log(err)
))
fs.writeFileSync("../xloss.txt",JSON.stringify(xloss_win),((err) ->
    if (err)
        log(err)))
fs.writeFileSync("../wloss.txt",JSON.stringify(wloss_win),((err) ->
    if (err)
        log(err)))
fs.writeFileSync("../train_acc.txt",JSON.stringify(train_acc_win),((err) ->
    if (err)
        log(err)))

log("Testing network.")
losses = []
s = 0
percentage_bar((if samples <= test.length then samples else test.length),((i)->
    x = train[i]
    net.forward(x.data)
    pred = net.getPrediction()
    loss = net.backward(x.class)
    losses.push(loss)
    s += pred == x.class ? 1:0;
    log("predicted:#{pred},actual:#{x.class}")
))
log("accuracy:#{(s/samples)*100}%,s:#{s}")
log(JSON.stringify(losses))
fs.writeFile("../losses_test.txt",JSON.stringify(losses),((err)->
    if (err)
        log(err)
))
