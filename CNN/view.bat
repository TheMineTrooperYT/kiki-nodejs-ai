IF '%1'=='avg' (
SET train="losses_train.txt.avg"
SET test="losses_test.txt.avg"
) ELSE (
SET train="losses_train.txt"
SET test="losses_test.txt"
)
py view.py %train% %test%