import matplotlib.pyplot as plt
import numpy as np
fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
bars = [0,1,2,3,4,5,6,7,8,9]
values = [5923,6742,5958,6131,5842,5421,5918,6265,5851,5949]
ax.bar(bars,values)
plt.show()
