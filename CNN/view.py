import matplotlib.pyplot as plt
import numpy as np
import sys
if len(sys.argv) == 1:
	path1 = input("enter file1 name: ")
	path2 = input("enter file2 name: ")
else:
	path1 = sys.argv[1]
	path2 = sys.argv[2]
	
f = open(path1)
exec("g1=np.array("+f.read()+")")
f.close()
f = open(path2)
exec("g2=np.array("+f.read()+")")
f.close()

print("1-green,2-red")
ax = plt.subplot(1,1,1)
xs=np.arange(len(g1))
ax.plot(xs,g1,"g-",xs,g2,"r-")
plt.show()
