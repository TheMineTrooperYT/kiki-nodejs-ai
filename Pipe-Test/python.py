import sys, socket, json, io
from contextlib import redirect_stdout

EVAL = "eval"
EXEC = "exec"
RUN  = "run"
EXIT = "exit"
SUCCESS = 0
FAILUR  = 1

# captures the stdout/err stream around the given command (retruns (ret_msg,ret_code))
def capture_stdout_err(exec_data):
    ret = "" # init the ret value
    ret_code = SUCCESS # init the ret code
    with io.StringIO() as buff, redirect_stdout(buff): # open buffer, redirect stdout
        err = "" # init error val
        try: # catch stderr stream
            exec(exec_data) # run code
        except Exception as e: # catch any exception
            err = "ERROR: " + str(e) # set err str
            ret_code = FAILUR # set ret code
        val = buff.getvalue() # get value
        ret += val[:-1] if val[-1] == "\n" else val  # add buffer (remove trailing \n if exists)
        ret += err # add error (empty if no error)
    
    return {"data":ret, "code":ret_code} # return item

# initializes the server socket (returns socket.socket)
def init_server():
    file = open("port") # open port file
    PORT = int(file.read()) # read port
    file.close() # close file
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # init socket
    
    sock.bind(('127.0.0.1',PORT)) # bind to port
    
    sock.listen(1) # set to listen
    
    return sock # return socket

# gets a client socket (returns a socket.socket)
def get_client(sock):
     sk,addr = sock.accept() # get socket,addr
     return sk # return socket

# pipes to the given socket
def Pipe(sk):
    run = True # saves the run state
    while run: # as long as not exit
        msg = eval(sk.recv(2**32).decode()) # eval the msg dictionary
        print("recived action: " + str(msg)) # print to screen
        _type = '' # init
        if not msg.__contains__('type'): # if map doesnt have a type
            _type = RUN #default to run
        else: # has type
            _type = msg['type'].lower() # get type
            
        ret_msg = {'code':SUCCESS,'data':''} # init ret msg
        if _type == EVAL: # if eval
            err = "" # init ret and err
            ret = ""
            try: # catch stderr
                exec("ret = str(" + str(msg['data']) + ")") # exec ret=str(expression)
            except Exception as e: # if error
                err = "ERROR: " + str(e) # set error
                ret_msg['code'] = FAILUR # set ret code
                
            ret_msg['data'] = ret + err # set msg
        elif _type == EXEC: # if exec
            ret_msg = capture_stdout_err(msg['data']) # get data,code from func
        elif _type == RUN: # if run
            err = "" # retrive only error
            try: # catch stderr
                exec(msg['data']) # exec data
            except Exception as e: # catch error
                err = "ERROR: " + str(e) # set err string
                ret_msg['code'] = FAILUR # set return code
            ret_msg['data'] = err # set data as err
        elif _type == EXIT: # if exit
            run = False # set to exit
            
        msg = json.dumps(ret_msg) # dump the map into string
        sk.sendall(msg.encode()) # send encoded msg
        print("replied to client: " + msg) # print to screen
    return

Pipe(get_client(init_server()))



    