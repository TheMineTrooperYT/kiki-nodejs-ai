var net = require('net'),
    fs  = require('fs'),
    assert = require('assert');

c = process.argv[process.argv.length-1];
c = Number(c);
if ( isNaN(c) ){
    c = 1;
}

global.reacived_data = "";
global.st = 0;
global.et = 0;
// initializes and connects the socket (returns net.Socket)
function init_server() {
    sock = new net.Socket();
    
    port = Number(fs.readFileSync("port","utf8")); // read port and convert to number
    
    assert(port != 0); // assert port exists
    
    sock.connect(port); // connect to it
    
    sock.on('data',(d) => {
        recived_data = d.toString();
    });
    
    return sock;
}

var waitForData = (timeoutms) => new Promise((r, j)=>{
    var check = () => {
        if (global.recived_data != undefined)
            r();
        else if((timeoutms -= 100) < 0)
            j('timed out!');
        else
          setTimeout(check, 100);
    };
    setTimeout(check, 100);
});

async function writeMsg(sock,msg,type="run") {
    global.recived_data = undefined;
    sock.write(`{"type":"${type}","data":"${msg}"}`); // send data to server
    await waitForData(4500);
    global.et = get_t();
    return global.recived_data;
}

function get_t(){
    var t = process.hrtime();
    t = t[0] + t[1]/1e9;
    return t;
}

(async ()=>{
    sock = init_server();
    var i;
    for (i = 0; i < c; i++){
        console.log("sent message");
        global.st = get_t();
        msg = await writeMsg(sock,"print('hi')","exec");
        var t = global.et-global.st;
        console.log(msg + ", t:" + t);
    }
})();


